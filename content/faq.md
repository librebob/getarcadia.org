---
title: "FAQ"
date: 2021-08-19T21:15:13+10:00
draft: false
---

**Q: Is this an open source client for Steam?**

A: No.

**Q: Isn't this basically just a flathub frontend?**

A: Yes, for now.

**Q: Why the new name?**

A: The last name was inaccessible.