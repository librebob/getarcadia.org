---
title: "Arcadia"
subtitle: "A GNU way to play"
date: 2021-08-19T21:15:13+10:00
draft: false
---

### A Steam-like platform for libre games

##### Features like these aren't just for proprietary platforms anymore

<span id="links">
<p id="reviews" class="c-hand">&raquo; <span class="menu-text">Reviews</span></p>
<p id="servers" class="c-hand">&raquo; <span class="menu-text">Server Browser</span></p>
<p id="tags" class="c-hand">&raquo; <span class="menu-text">Tag filtering</span></p>
<p id="grid" class="c-hand">&raquo; <span class="menu-text">Grid View</span></p>
<p>&raquo; Much more to come!</p>
<span>