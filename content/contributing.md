---
title: "Contributing"
date: 2021-08-19T21:15:13+10:00
draft: false
---

### Translations

We are proudly supported by weblate https://hosted.weblate.org/projects/athenaeum/translations/

### Gitlab

Join us at https://gitlab.com/librebob/athenaeum

### Have ideas or just plain interested?

Get in touch https://matrix.to/#/#arcadia:matrix.org